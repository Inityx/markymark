use std::{
    cmp::Ordering,
    fmt,
};

#[derive(Clone, Copy, PartialEq, Eq)]
pub enum Token<'src> {
    Word(&'src str),
    End,
}

impl fmt::Debug for Token<'_> {
    fn fmt(&self, fmt: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Token::Word(w) => fmt::Debug::fmt(w, fmt),
            Token::End     => fmt.write_str("$"), 
        }
    }
}

#[derive(Clone, Copy)]
pub struct Link<'src> {
    pub token: Token<'src>,
    pub count: usize,
}

impl fmt::Debug for Link<'_> {
    fn fmt(&self, fmt: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(fmt, "({:?} => {})", self.token, self.count)
    }
}

impl<'src> Link<'src> {
    pub fn of(token: Token<'src>) -> Self {
        Link { token, count: 1 }
    }

    pub fn merge(&mut self, rhs: Self) {
        debug_assert!(rhs.token == self.token);
        self.count += rhs.count;
    }
}

impl<'a> PartialEq for Link<'a> {
    fn eq(&self, rhs: &Self) -> bool {
        self.count.eq(&rhs.count)
    }
}

impl Eq for Link<'_> {}

impl PartialOrd for Link<'_> {
    fn partial_cmp(&self, rhs: &Self) -> Option<Ordering> {
        Some(self.cmp(rhs))
    }
}

impl Ord for Link<'_> {
    fn cmp(&self, rhs: &Self) -> Ordering {
        self.count.cmp(&rhs.count)
    }
}
