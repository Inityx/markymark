use std::{
    fmt,
    ops::{Deref, DerefMut},
};

use super::{Link, Token};

#[derive(Default)]
pub struct Candidates<'src>(Vec<Link<'src>>);

impl<'src> Deref for Candidates<'src> {
    type Target = Vec<Link<'src>>;

    fn deref(&self) -> &Self::Target {
        let Candidates(vector) = self;
        vector
    }
}

impl DerefMut for Candidates<'_> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        let Candidates(vector) = self;
        vector
    }
}

impl<'src> Candidates<'src> {
    fn existing(&mut self, token: Token<'_>) -> Option<&mut Link<'src>> {
        self.iter_mut().find(|l| l.token == token)
    }

    pub fn add(&mut self, token: Token<'src>) {
        let link = Link::of(token);

        if let Some(existing) = self.existing(token) {
            existing.merge(link);
            self.sort_unstable_by(|a, b| b.cmp(a)); // reverse
        } else {
            self.push(link);
        }
    }
}

impl fmt::Debug for Candidates<'_> {
    fn fmt(&self, fmt: &mut fmt::Formatter<'_>) -> fmt::Result {
        fmt.write_str("[")?;
        let mut iter = self.iter();

        if let Some(link) = iter.next() {
            fmt::Debug::fmt(link, fmt)?;
        }

        for link in iter {
            fmt.write_str(", ")?;
            fmt::Debug::fmt(link, fmt)?;
        }

        fmt.write_str("]")
    }
}
