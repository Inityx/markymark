#![feature(uniform_paths, pattern, unboxed_closures, rust_2018_preview)]

mod markov;

use std::{env, fs, io, time, process::exit};
use rand::thread_rng;
use markov::MarkovGenerator;

const SENTENCE_DELIMITERS: &str = ".?!";

fn read_files(filenames: impl IntoIterator<Item=String>) -> Vec<String> {
    eprintln!("Reading files...");
    filenames
        .into_iter()
        .map(fs::read_to_string)
        .collect::<Result<_,_>>()
        .unwrap()
}

fn train(depth: usize, sources: &[String]) -> MarkovGenerator<'_> {
    fn stopwatch(timed: impl FnOnce()) -> time::Duration {
        let time = time::Instant::now();
        timed();
        time.elapsed()
    }

    fn microsecond_time(d: time::Duration) -> f32 {
        d.as_secs() as f32 + (d.subsec_micros() as f32 / 1_000_000f32)
    }

    fn sentence_end(c: char) -> bool { SENTENCE_DELIMITERS.contains(c) }

    eprintln!("Training chain...");
    let mut markov = MarkovGenerator::with_depth(depth);

    let duration = stopwatch(|| {
        for source in sources {
            markov.train_text(source, sentence_end);
        }
    });

    let (contexts, links) = markov.num_contexts_links();
    eprintln!(
        "Trained {} contexts and {} links in {} seconds.",
        contexts, links, microsecond_time(duration),
    );

    markov
}

fn usage() -> ! {
    eprintln!("Usage: markymark depth file1 [file2]...");
    exit(1);
}

fn main() {
    if env::args().count() < 3 { usage() }

    let depth = env::args()
        .nth(1)
        .unwrap()
        .parse::<usize>()
        .unwrap_or_else(|_| usage());
    
    let sources = read_files(env::args().skip(2));
    let markov = train(depth, &sources);
    let mut rng = thread_rng();

    eprintln!("Press enter for a new sentence.");

    loop {
        io::stdin().read_line(&mut String::new()).unwrap();
        println!("{}", markov.generate_sentence(&mut rng));
    }
}
